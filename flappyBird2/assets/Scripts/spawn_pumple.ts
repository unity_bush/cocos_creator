
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    pumplePrefab: cc.Prefab = null;

    @property(cc.Node)
    pumpLayer:cc.Node=null

    onLoad () {
        this.node.on('startGame',this.startGame,this);
    }

    start () {

    }

    startGame(){
      this.schedule(this.spawnPumple,1);
    }


    spawnPumple(){
        let pumple=cc.instantiate(this.pumplePrefab);
        this.pumpLayer.addChild(pumple);
        pumple.emit('init');
    }

    // update (dt) {}
}
