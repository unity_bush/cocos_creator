const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
	// LIFE-CYCLE CALLBACKS:

	// onLoad () {}

	start() {
		this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
		this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
		this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
		this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchEnd, this);
	}

	tempPos: cc.Vec3 = cc.v3(0, 0);
	touchStart(e: cc.Event.EventTouch) {
		this.tempPos = this.node.position;
	}

	touchMove(e: cc.Event.EventTouch) {
		let delta = cc.v3(e.getDelta()).add(this.node.position);
		this.node.position = delta;
	}

	touchEnd(e: cc.Event.EventTouch) {
		this.node.position = this.tempPos;
	}
}
